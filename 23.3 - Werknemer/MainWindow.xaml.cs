﻿using System.Windows;

namespace _23._3___Werknemer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        List<Werknemer> Lijstwerknemers = new List<Werknemer>();
        private void BtnToevoegen_Click(object sender, RoutedEventArgs e)
        {
            Werknemer werknemer = new Werknemer(txtAchternaam.Text, txtVoornaam.Text, Convert.ToDouble(txtVerdiensten.Text));
            Lijstwerknemers.Add(werknemer);
            //foreach (Werknemer eenWerknemer in Lijstwerknemers)
            //{
            //    lbDisplay.Items.Add(eenWerknemer.VolledigeWeergave);
            //}
            lbDisplay.ItemsSource = null;
            lbDisplay.ItemsSource = Lijstwerknemers;
            txtAchternaam.Clear();
            txtVoornaam.Clear();
            txtVerdiensten.Clear();
        }

        private void Window_Loaded()
        {

        }
    }
}